import React from 'react';
import PropTypes from 'prop-types';

TeamTable.propTypes = {
    teams: PropTypes.array
};

function TeamTable({teams}) {
    return (
        <table className="table table-striped">
            <thead className="thead-dark">
            <tr>
                <th>Team Name</th>
            </tr>
            </thead>
            <tbody>
            {teams.length === 0 && <tr><td>There are no teams</td></tr>}
            {teams.length > 0 && teams.map(t => {
                return <tr key={t.id}><td>{t.name}</td></tr>
            })}
            </tbody>
        </table>
    );
}

TeamTable.defaultProps = {
  teams: []
};

export default TeamTable;
