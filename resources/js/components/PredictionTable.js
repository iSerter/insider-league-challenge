import React from 'react';
import PropTypes from 'prop-types';

PredictionTable.propTypes = {
    predictions: PropTypes.array
};

function PredictionTable({predictions}) {
    return (
        <>
            <table className="table table-striped">
                <thead className="thead-dark">
                <tr>
                    <th>Championship Predictions</th>
                    <th>%</th>
                </tr>
                </thead>
                <tbody>
                {predictions.length > 0 && predictions.map((p, i) => {
                    return <tr key={i}>
                        <td>{p.team.name}</td>
                        <td><small>{p.odds}%</small></td>
                    </tr>
                })}
                </tbody>
            </table>
        </>
    );
}

export default PredictionTable;
