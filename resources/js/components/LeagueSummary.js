import React from 'react';
import PropTypes from 'prop-types';

LeagueSummary.propTypes = {
    teams: PropTypes.array
};

function LeagueSummary({teams}) {
    return (
        <>
            <table className="table table-striped">
                <thead className="thead-dark">
                <tr>
                    <th>Team Name</th>
                    <th>P</th>
                    <th>W</th>
                    <th>D</th>
                    <th>L</th>
                    <th>GD</th>
                </tr>
                </thead>
                <tbody>
                {teams.length > 0 && teams.map(t => {
                    return <tr key={t.id}>
                        <td>{t.name}</td>
                        <td>{t.summary.points}</td>
                        <td>{t.summary.wins}</td>
                        <td>{t.summary.draws}</td>
                        <td>{t.summary.loses}</td>
                        <td>{t.summary.goal_difference}</td>
                    </tr>
                })}
                </tbody>
            </table>
        </>
    );
}

LeagueSummary.defaultProps = {
    teams: [],
};

export default LeagueSummary;
