import React from 'react';
import PropTypes from 'prop-types';

WeekTable.propTypes = {
    week: PropTypes.any,
    fixtures: PropTypes.array
};

function WeekTable({week, fixtures}) {
    return (
        <table className="table table-striped">
            <thead className="thead-dark">
            <tr>
                <th colSpan={3}>Week {week}</th>
            </tr>
            </thead>
            <tbody>
            {fixtures.map(f => {
                return <tr key={f.id}>
                    <td>{f.home_team.name}</td>
                    <td>-</td>
                    <td>{f.away_team.name}</td>
                </tr>
            })}
            </tbody>
        </table>
    );
}

export default WeekTable;
