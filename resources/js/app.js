import React from "react";
import Routes from "./routes";
import ReactDOM from "react-dom";
import SnackbarProvider from 'react-simple-snackbar'
import {LoadingProvider} from "./contexts/loading";

function App() {
    return (
        <LoadingProvider>
            <SnackbarProvider>
                <div className="container">
                    <div className="justify-content-center">
                        <Routes/>
                    </div>
                </div>
            </SnackbarProvider>
        </LoadingProvider>
    );
}

if (document.getElementById('root')) {
    ReactDOM.render(<App/>, document.getElementById('root'));
}
