import React, {useContext, useState} from 'react';
import PropTypes from 'prop-types';
import LoadingOverlay from 'react-loading-overlay';

export const LoadingContext = React.createContext();

export const useLoading = () => useContext(LoadingContext);

export function LoadingProvider(props) {
    const [loading, setLoading] = useState(false);

    return (
        <LoadingContext.Provider
            value={{
                loading: loading,
                showLoading: () => setLoading(true),
                hideLoading: () => setLoading(false)
            }}>
            <LoadingOverlay
                active={loading}
                spinner
                text='Loading...'
            >
                {props.children}
            </LoadingOverlay>
        </LoadingContext.Provider>
    );
}

LoadingProvider.propTypes = {
    children: PropTypes.node
};
