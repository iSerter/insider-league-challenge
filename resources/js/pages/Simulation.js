import React, {useEffect, useState} from 'react';
import axios from "axios";
import LeagueSummary from "../components/LeagueSummary";
import WeekTable from "../components/WeekTable";
import PredictionTable from "../components/PredictionTable";
import {useHistory} from "react-router-dom";

function Simulation(props) {

    const history = useHistory();

    const [summary, setSummary] = useState(null);

    useEffect(() => {
        axios.get('/api/league/summary').then(r => {
            setSummary(r.data);
        });
    }, []);

    const play = (week) => {
        axios.post('/api/league/simulate', {week})
            .then(r => {
                setSummary(r.data);
            });
    };

    const resetData = () => {
        axios.post('/api/league/reset')
            .then(r => {
                history.push('/fixtures/generate');
            });
    }

    return (
        <>
            <h1 style={{marginBottom: 40}}>Simulation</h1>

            {summary && (
                <>
                    <div className="row">
                        <div className="col-md-6">
                            <LeagueSummary teams={summary?.teams_summary}/>
                        </div>
                        <div className="col-md-3">
                            <WeekTable week={summary.week_summary.week} fixtures={summary.week_summary.fixtures}/>
                        </div>
                        <div className="col-md-3">
                            <PredictionTable predictions={summary.championship_prediction}/>
                        </div>
                    </div>

                    <hr style={{marginBottom: 20}}/>

                    <div className="row">
                        <div className="col-sm-4">
                            <button disabled={!(summary.week_summary.week < summary.last_week)}
                                    className="btn btn-primary" onClick={() => play(null)}>
                                Play All Weeks
                            </button>
                        </div>
                        <div className="col-sm-4">
                            <button disabled={!(summary.week_summary.week < summary.last_week)}
                                    className="btn btn-primary" onClick={() => play(summary.week_summary.week + 1)}>
                                Play Next Week
                            </button>
                        </div>
                        <div className="col-sm-4">
                            <button className="btn btn-danger" onClick={resetData}>Reset Data</button>
                        </div>
                    </div>
                </>
            )}

        </>
    );
}

export default Simulation;
