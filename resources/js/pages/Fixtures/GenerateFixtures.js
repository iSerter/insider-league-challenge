import React, {useEffect, useState} from 'react';
import axios from "axios";
import TeamTable from "../../components/TeamTable";
import {useHistory} from "react-router-dom";
import {useSnackbar} from 'react-simple-snackbar';

function GenerateFixtures(props) {
    const history = useHistory();
    const [openSnackbar, closeSnackbar] = useSnackbar();

    const [teams, setTeams] = useState([]);

    useEffect(() => {
        axios.get('/api/teams').then(r => {
            console.log('teamsData', r.data);
            setTeams([...r.data]);
        });
    }, [])

    const generateFixtures = () => {
        axios.post('/api/league/generate').then(r => {
            history.push('/fixtures/list');
        }).catch(e => {
            // console.log('req error', e, e.data);
            openSnackbar(e.response?.data?.message || 'Something went wrong');
        });
    };

    return (
        <>
            <h1 style={{marginBottom: 40}}>Generate Fixtures Page</h1>


            <h2>Teams</h2>
            <TeamTable teams={teams}/>
            <br/>
            <button className="btn btn-primary" onClick={generateFixtures}>Generate Fixtures</button>
        </>
    );
}

export default GenerateFixtures;
