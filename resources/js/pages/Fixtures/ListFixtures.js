import React, {useEffect, useState} from 'react';
import axios from "axios";
import _ from 'lodash';
import WeekTable from "../../components/WeekTable";
import {useHistory} from "react-router-dom";

function ListFixtures(props) {
    const history = useHistory();

    const [fixtures, setFixtures] = useState([]);

    useEffect(() => {
        axios.get('/api/league/fixtures').then(r => {
            setFixtures([..._.values(_.groupBy(r.data, 'league_week'))]);
        });
    }, []);

    const simulate = () => {
        axios.post('/api/league/simulate', {week: 1}).then(() => {
            history.push('/simulation');
        });
    }

    return (
        <>
            <h1 style={{marginBottom: 40}}>Fixture List</h1>
            <div className="row">
                {fixtures.length > 0 && fixtures.map(f => {
                    return <div key={f.id} className="col-sm-3">
                        <WeekTable week={f[0].league_week} fixtures={f}/>
                    </div>;
                })}
            </div>

            <button className="btn btn-primary" onClick={simulate}>Start Simulation</button>
        </>
    );
}

export default ListFixtures;
