import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import GenerateFixtures from "./pages/Fixtures/GenerateFixtures";
import ListFixtures from "./pages/Fixtures/ListFixtures";
import Simulation from "./pages/Simulation";

export default function Routes() {
    return (
        <Router>
            <Switch>
                <Route path="/fixtures/generate"><GenerateFixtures/></Route>
                <Route path="/fixtures/list"><ListFixtures/></Route>
                <Route path="/simulation"><Simulation/></Route>

                <Route path="/">
                    <GenerateFixtures/>
                </Route>
            </Switch>
        </Router>
    );
}
