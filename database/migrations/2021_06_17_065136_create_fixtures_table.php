<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'fixtures',
            function (Blueprint $table) {
                $table->id();

//                $table->unsignedBigInteger('league_id');
//                $table->foreign('league_id')->references('id')->on('leagues');

                $table->unsignedInteger('league_week');

                $table->unsignedBigInteger('home_team_id');
                $table->foreign('home_team_id')->references('id')->on('teams');

                $table->unsignedBigInteger('away_team_id');
                $table->foreign('away_team_id')->references('id')->on('teams');

//                $table->unique(['home_team_id', 'away_team_id']);
//                $table->timestamp('start_time');

                $table->unsignedInteger('home_team_score')->nullable();
                $table->unsignedInteger('away_team_score')->nullable();

                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixtures');
    }
}
