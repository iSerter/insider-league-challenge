<?php

namespace Database\Factories;

use App\Models\Fixture;
use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class FixtureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Fixture::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $homeTeam = Team::query()->inRandomOrder()->first();
        $awayTeam = $homeTeam ? Team::query()->where('id', '!=', $homeTeam->id)->first() : null;
        return [
            'home_team_id' => $homeTeam?->id,
            'away_team_id' => $awayTeam?->id,
            'starts_at' => now()->addWeek(),
            'home_team_score' => 0,
            'away_team_score' => 0,
        ];
    }
}
