<?php

namespace Database\Factories;

use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Team::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
//        $resourceFile = database_path('data/teams/england-football-teams.json');
//        $resource = json_decode(file_get_contents($resourceFile), true);
//        shuffle($resource['clubs']);
        return [
            'name' => $this->faker->name,
        ];
    }
}
