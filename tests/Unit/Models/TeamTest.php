<?php

namespace Tests\Unit\Models;

use App\Models\Team;
use App\Services\LeagueService;
use Tests\TestCase;

class TeamTest extends TestCase
{
    public function testTeamFixturesAttribute()
    {
        $team = Team::query()->first();
        $leagueService = app(LeagueService::class);
        $leagueService->createFixtures(2);
        $this->assertCount(6, $team->fixtures);
    }
}
