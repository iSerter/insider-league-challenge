<?php

namespace Tests\Unit\Services;

use App\Models\Fixture;
use App\Models\Team;
use App\Services\LeagueService;
use Tests\TestCase;

class LeagueServiceTest extends TestCase
{
    protected LeagueService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = app(LeagueService::class);
    }

    public function testCalculateFixtureCount()
    {
        $this->assertEquals(2, $this->service->calculateFixtureCount(2));
        $this->assertEquals(6, $this->service->calculateFixtureCount(3));
        $this->assertEquals(12, $this->service->calculateFixtureCount(4));
        $this->assertEquals(20, $this->service->calculateFixtureCount(5));
    }

    public function testCalculateWeekCount()
    {
        $this->assertEquals(2, $this->service->calculateWeekCount(2, 1));

        // 3 teams = 6 fixtures = 3 weeks when fixturePerWeek=2
        $this->assertEquals(3, $this->service->calculateWeekCount(3, 2));

        // 4 teams = 12 fixtures = 4 weeks when fixturePerWeek=3
        $this->assertEquals(4, $this->service->calculateWeekCount(4, 3));

        // 5 teams = 20 fixtures = 7 weeks when fixturePerWeek=3 (only 2 fixtures last week)
        $this->assertEquals(7, $this->service->calculateWeekCount(5, 3));
    }


    public function matchTeamIds()
    {
        $teamIds = [1, 2, 3, 4];
        $matches = $this->service->matchTeams($teamIds);
        $this->assertCount(12, $matches);
        // @todo assert expected array.
    }

    public function testCreateFixtures()
    {
        // initial db has 4 teams atm.
        // there will be 12 fixtures..
        // lets test 2 fixtures per week, totalling 6 weeks.
        $fixtures = $this->service->createFixtures(2);
        $this->assertCount(12, $fixtures);
        $weekCount = Fixture::query()->select('league_week')->max('league_week');
        $this->assertEquals(6, $weekCount);
        for ($i = 1; $i <= $weekCount; $i++) {
            // assert there are exactly 2 matches per week..
            $this->assertEquals(2, Fixture::query()->where('league_week', '=', $i)->count());
        }
    }
}
