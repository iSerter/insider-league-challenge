<?php


namespace Tests\Feature;


use App\Models\Team;
use Tests\TestCase;

class TeamApiTest extends TestCase
{
    public function testGetTeams()
    {
        $response = $this->get('/api/teams');
        $response->assertStatus(200);
        $this->assertCount(Team::query()->count(), $response->json());
    }
}
