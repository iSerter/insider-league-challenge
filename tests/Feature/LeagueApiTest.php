<?php


namespace Tests\Feature;


use App\Entities\TeamSummary;
use App\Models\Fixture;
use App\Models\Team;
use Tests\TestCase;

class LeagueApiTest extends TestCase
{
    public function testLeagueGenerate()
    {
        $this->assertEmpty(Fixture::all());
        $response = $this->post('/api/league/generate');
        $response->assertStatus(200);
        $this->assertNotEmpty(Fixture::all());
    }

    /**
     * @depends testLeagueGenerate
     */
    public function testGetLeagueFixtures()
    {
        $response = $this->get('/api/league/fixtures');
        $response->assertStatus(200);
        $this->assertCount(Fixture::query()->count(), $response->json());
    }

    /**
     * @depends testLeagueGenerate
     */
    public function testGetLeagueSummary()
    {
        $response = $this->get('/api/league/summary');
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'teams_summary' => [
                    ['summary' => []]
                ],
                'championship_prediction' => [],
            ]
        );
        $this->assertCount(Team::query()->count(), $response->json('teams_summary'));
    }
}
