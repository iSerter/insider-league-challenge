<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('teams', 'TeamController@index');

Route::post('league/generate', 'LeagueController@generate');
Route::get('league/fixtures', 'LeagueController@fixtures');
Route::get('league/summary', 'LeagueController@summary');
Route::post('league/simulate', 'LeagueController@simulate');
Route::post('league/reset', 'LeagueController@reset');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
