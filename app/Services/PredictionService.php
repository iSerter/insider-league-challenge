<?php

namespace App\Services;

use App\Models\Fixture;
use App\Models\Team;
use Illuminate\Database\Eloquent\Collection;

class PredictionService
{
    public function predict(Collection $teams)
    {
        $totalPoints = 0;
        $teams->each(
            function (Team $team) use (&$totalPoints) {
                $totalPoints += $team->summary->getPoints();
            }
        );
        return $teams->map(
            function (Team $team) use ($totalPoints) {
                return [
                    'team' => $team->only('id', 'name'),
                    'odds' => round(
                        $team->summary->getPoints() === 0 ? 0 : $team->summary->getPoints() / $totalPoints * 100,
                        2
                    )
                ];
            }
        )->sortBy('odds', SORT_REGULAR, true)->values();
    }
}
