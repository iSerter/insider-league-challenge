<?php

namespace App\Services;

use App\Models\Fixture;
use App\Models\Team;

class LeagueService
{
    public function createFixtures($fixturePerWeek = 1)
    {
        if (Team::query()->count() < 2) {
            throw new \RuntimeException('There are not enough number of teams to create fixtures.');
        }
        if (Fixture::query()->exists()) {
            throw new \RuntimeException('Fixtures are already created');
        }
        $teams = Team::query()->get();
        $weekCount = $this->calculateWeekCount($teams->count(), $fixturePerWeek);
        $matches = $this->matchTeamIds($teams->pluck('id')->toArray());
        $fixtures = [];
        for ($week = 1; $week <= $weekCount; $week++) {
            $matchesForThisWeek = array_splice($matches, 0, $fixturePerWeek);
            foreach ($matchesForThisWeek as $match) {
                $fixtureData = ['league_week' => $week, 'home_team_id' => $match[0], 'away_team_id' => $match[1]];
                $fixtures[] = Fixture::query()->create($fixtureData);
            }
        }
        return $fixtures;
    }

    public function matchTeamIds(array $teamIds): array
    {
        $permutations = new \drupol\phpermutations\Generators\Permutations($teamIds, 2);
        return $permutations->toArray();
    }

    public function calculateWeekCount(int $teamCount, int $fixturePerWeek): int
    {
        $combinationCount = $this->calculateFixtureCount($teamCount);
        $weekCount = $combinationCount / $fixturePerWeek;
        return ceil($weekCount);
    }

    public function calculateFixtureCount(int $teamCount): int
    {
        if ($teamCount < 2) {
            throw new \InvalidArgumentException("Team count cannot be less than 2");
        }
        if ($teamCount === 2) {
            return 2;
        }
        return $teamCount * 2 * ($teamCount - 1) / 2;
    }
}
