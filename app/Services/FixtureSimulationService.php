<?php


namespace App\Services;

use App\Models\Fixture;

class FixtureSimulationService
{
    public function simulate(Fixture $fixture)
    {
        $fixture->home_team_score = rand(0, 5);
        $fixture->away_team_score = rand(0, 5);
        $fixture->save();
    }

    public function simulateWeek(int $week)
    {
        Fixture::query()->where('league_week', '=', $week)
            ->get()
            ->each(fn(Fixture $fixture) => $this->simulate($fixture));
    }

    public function simulateAll()
    {
        Fixture::query()->whereNull('home_team_score')
            ->get()
            ->each(fn(Fixture $fixture) => $this->simulate($fixture));
    }
}
