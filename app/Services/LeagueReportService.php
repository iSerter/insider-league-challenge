<?php

namespace App\Services;

use App\Entities\TeamSummary;
use App\Models\Fixture;
use App\Models\Team;

class LeagueReportService
{
    public function getTeamSummary(Team $team): TeamSummary
    {
        $summary = (new TeamSummary());
        /** @var Fixture $fixture */
        foreach ($team->fixtures->filter(fn(Fixture $f) => $f->isFinished()) as $fixture) {
            if ($fixture->isDraw()) {
                $summary->setDraws($summary->getDraws() + 1);
                $summary->setPoints($summary->getPoints() + 1);
            } elseif ($team->isWinner($fixture)) {
                $summary->setWins($summary->getWins() + 1);
                $summary->setPoints($summary->getPoints() + 3);
                // @todo confirm goal difference calculation
                $summary->setGoalDifference($summary->getGoalDifference() + $fixture->getGoalDifference());
            } else {
                $summary->setLoses($summary->getLoses() + 1);
            }
        }

        return $summary;
    }
}
