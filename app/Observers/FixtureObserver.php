<?php

namespace App\Observers;

use App\Models\Fixture;

class FixtureObserver
{
    public function updating(Fixture $fixture)
    {
        if($fixture->isDirty('home_team_score') || $fixture->isDirty('home_team_score')) {
            Fixture::updated(function(Fixture $f) {
                $f->homeTeam->flushSummaryCache();
                $f->awayTeam->flushSummaryCache();
            });
        }
    }
}
