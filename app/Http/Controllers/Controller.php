<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;


    public function ok($data, $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json($data, $code);
    }

    public function error($message, $code = 422, array $data = null): \Illuminate\Http\JsonResponse
    {
        $data = array_merge(['message' => $message], $data ?? []);
        return response()->json(
            $data,
            $code
        );
    }
}
