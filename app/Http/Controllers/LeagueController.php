<?php

namespace App\Http\Controllers;

use App\Models\Fixture;
use App\Models\League;
use App\Models\Team;
use App\Services\FixtureSimulationService;
use App\Services\LeagueReportService;
use App\Services\LeagueService;
use App\Services\PredictionService;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    public function generate(LeagueService $leagueService)
    {
        try {
            $leagueService->createFixtures(2);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        return $this->ok(Fixture::query()->get());
    }

    public function fixtures()
    {
        return $this->ok(Fixture::query()->get());
    }

    public function summary(Request $request, PredictionService $predictionService)
    {
        $teams = Team::query()->get();
        $predictions = $predictionService->predict($teams);
        $teamsWithSummary = $teams->map(
            function (Team $t) {
                return $t->toArray() + ['summary' => $t->summary];
            }
        );
        $week = Fixture::whereNotNull('home_team_score')->max('league_week') ?? 1;
        $weekFixtures = [
            'week' => $week,
            'fixtures' => Fixture::query()->where('league_week', '=', $week)->get()
        ];
        return [
            'teams_summary' => $teamsWithSummary,
            'week_summary' => $weekFixtures,
            'championship_prediction' => $predictions,
            'last_week' => Fixture::query()->max('league_week')
        ];
    }

    public function simulate(Request $request, FixtureSimulationService $service)
    {
        $week = $request->input('week');
        if (!$week) {
            $service->simulateAll();
        } else {
            $service->simulateWeek($week);
        }
        return $this->summary($request, app(PredictionService::class), app(LeagueReportService::class));
    }

    public function reset()
    {
        Fixture::query()->delete();
        Team::query()->select('id')->get()->each(fn(Team $t) => $t->flushSummaryCache());
        return $this->ok(null, 201);
    }
}
