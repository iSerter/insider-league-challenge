<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class TeamController extends Controller
{

    public function index()
    {
        $teams = QueryBuilder::for(Team::class)
            ->get();

        return $this->ok($teams);
    }
}
