<?php

namespace App\Entities;

use Illuminate\Contracts\Support\Arrayable;

class TeamSummary implements Arrayable, \JsonSerializable
{
    protected int $points = 0;
    protected int $wins = 0;
    protected int $draws = 0;
    protected int $loses = 0;
    protected int $goalDifference = 0;


    public function toArray(): array
    {
        return [
            'points' => $this->getPoints(),
            'wins' => $this->getWins(),
            'draws' => $this->getDraws(),
            'loses' => $this->getLoses(),
            'goal_difference' => $this->getGoalDifference(),
        ];
    }


    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Getters/Setters
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return TeamSummary
     */
    public function setPoints(int $points): TeamSummary
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return int
     */
    public function getWins(): int
    {
        return $this->wins;
    }

    /**
     * @param int $wins
     * @return TeamSummary
     */
    public function setWins(int $wins): TeamSummary
    {
        $this->wins = $wins;
        return $this;
    }

    /**
     * @return int
     */
    public function getDraws(): int
    {
        return $this->draws;
    }

    /**
     * @param int $draws
     * @return TeamSummary
     */
    public function setDraws(int $draws): TeamSummary
    {
        $this->draws = $draws;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoses(): int
    {
        return $this->loses;
    }

    /**
     * @param int $loses
     * @return TeamSummary
     */
    public function setLoses(int $loses): TeamSummary
    {
        $this->loses = $loses;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoalDifference(): int
    {
        return $this->goalDifference;
    }

    /**
     * @param int $goalDifference
     * @return TeamSummary
     */
    public function setGoalDifference(int $goalDifference): TeamSummary
    {
        $this->goalDifference = $goalDifference;
        return $this;
    }

}
