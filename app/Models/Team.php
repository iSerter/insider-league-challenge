<?php

namespace App\Models;

use App\Entities\TeamSummary;
use App\Services\LeagueReportService;
use Illuminate\Database\Eloquent\Collection;

/**
 * App\Models\Team
 *
 * @property-read Collection $fixtures
 * @property-read TeamSummary $summary
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fixture[] $awayFixtures
 * @property-read int|null $away_fixtures_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fixture[] $homeFixtures
 * @property-read int|null $home_fixtures_count
 * @method static \Illuminate\Database\Eloquent\Builder|Team newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUpdatedAt($value)
 */
class Team extends BaseModel
{
    protected $table = 'teams';
    protected $guarded = [];


    /*
    |--------------------------------------------------------------------------
    | helpers
    |--------------------------------------------------------------------------
    */

    public function isWinner(Fixture $fixture): bool
    {
        return $fixture->isFinished() && !$fixture->isDraw() && $fixture->getWinner()?->id === $this->id;
    }

    public function flushSummaryCache()
    {
        cache()->forget('teams:' . $this->id . ':summary');
    }

    /*
    |--------------------------------------------------------------------------
    | getters
    |--------------------------------------------------------------------------
    */

    public function getFixturesAttribute(): Collection
    {
        return $this->homeFixtures()->union($this->awayFixtures()->getBaseQuery())->get();
    }

    public function getSummaryAttribute(): TeamSummary
    {
        return cache()->remember(
            'teams:' . $this->id . ':summary',
            300,
            function () {
                /** @var LeagueReportService $service */
                $service = app(LeagueReportService::class);
                return $service->getTeamSummary($this);
            }
        );
    }

    /*
    |--------------------------------------------------------------------------
    | relations
    |--------------------------------------------------------------------------
    */
    public function homeFixtures()
    {
        return $this->hasMany(Fixture::class, 'home_team_id', 'id');
    }

    public function awayFixtures()
    {
        return $this->hasMany(Fixture::class, 'away_team_id', 'id');
    }
}
