<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Fixture
 *
 * @property-read \App\Models\Team $awayTeam
 * @property-read \App\Models\Team $homeTeam
 * @method static \Database\Factories\FixtureFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $league_week
 * @property int $home_team_id
 * @property int $away_team_id
 * @property int $home_team_score
 * @property int $away_team_score
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereAwayTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereAwayTeamScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereHomeTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereHomeTeamScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereLeagueWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereUpdatedAt($value)
 */
class Fixture extends Model
{
    use HasFactory;

    protected $table = 'fixtures';
    protected $guarded = [];
    protected $with = ['homeTeam', 'awayTeam'];
    protected $hidden = ['home_team_id', 'away_team_id'];

    /*
    |--------------------------------------------------------------------------
    | helpers
    |--------------------------------------------------------------------------
    */
    public function isFinished(): bool
    {
        return $this->home_team_score !== null;
    }

    public function isDraw(): bool
    {
        return $this->isFinished() && $this->home_team_score === $this->away_team_score;
    }

    public function getWinner(): ?Team
    {
        if (!$this->isFinished() || $this->isDraw()) {
            return null;
        }

        return $this->home_team_score > $this->away_team_score ? $this->homeTeam : $this->awayTeam;
    }

    public function getGoalDifference(): int
    {
        if (!$this->isFinished()) {
            return 0; // not so happy with this..
        }
        return $this->homeTeam->isWinner($this)
            ? $this->home_team_score - $this->away_team_score
            : $this->away_team_score - $this->home_team_score;
    }

    /*
    |--------------------------------------------------------------------------
    | relations
    |--------------------------------------------------------------------------
    */
    public function homeTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'home_team_id', 'id');
    }

    public function awayTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'away_team_id', 'id');
    }
}
